# LowBudgetBetBack

## Prérequis
- [Java 8.0 (JDK/JRE)](https://www.oracle.com/fr/java/technologies/javase/javase-jdk8-downloads.html)
- [MySQL 8.0](https://dev.mysql.com/downloads/mysql/)
- [Eclipse](https://www.eclipse.org/downloads/)

## Démarrage

### Cloner le projet

Entrer la commande `git clone git@gitlab.com:Zarhkoh/lowbudgetbetback.git` pour cloner le projet.


### Préparation de la base de données

Afin d'avoir un pool de données utilisables, nous recommandons d'utiliser les données fournies dans /assets/sql_mock

- créer une base de données appelée "low_budget_bet"
- importer les tables et les données des fichiers sql `low_budget_bet_hibernate_sequence.sql` et `low_budget_bet_hibernate_utilisateur.sql`.


### Installation des dépendances

Ouvrir le projet à l'aide d'Eclipse.
Clic droit sur le projet puis `Run As > Maven Install` afin d'installer les dépendances nécessaires au projet.


### Lancer le projet
clic droit sur LbbBackApplication.java puis `Run As > Java Application` afin de lancer le projet.





