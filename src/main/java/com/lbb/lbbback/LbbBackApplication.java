package com.lbb.lbbback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LbbBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(LbbBackApplication.class, args);
	}

}
