package com.lbb.lbbback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lbb.lbbback.models.User;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByEmail(String email);
    User findByParrain(String parrain);
    User findByPseudo(String pseudo);


}
