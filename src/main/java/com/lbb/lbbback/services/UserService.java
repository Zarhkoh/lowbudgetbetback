package com.lbb.lbbback.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.lbb.lbbback.models.User;
import com.lbb.lbbback.repositories.UserRepository;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	public List<User> getAllUsers() {
        return userRepository.findAll();
	}

	//Récupère un utilisateur par son ID
	public User getUserById(Integer id) {
		return userRepository.findById(id).get();
	}
	
	//Récupère un utilisateur par son email
	public User getUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	
	//Récupère un utilisateur par son email ET compare le password (simule le login)
	public User getUserByEmailAndPassword(String email, String password) {
		User user = userRepository.findByEmail(email);
		if(user != null && user.getPassword().equals(password)) {
			return user;
		}
		throw new Error("Combinaison email/mot de passe invalide.");
	}
	
	 public void saveUser(User user) {
		User parrain = userRepository.findByPseudo(user.getParrain());
		 if(user.getEmail()== null || user.getPassword()==null || user.getPseudo()== null || user.getNom()==null || user.getPrenom()==null) {
	         throw new Error("Tous les champs doivent être remplis");
		 }
		 if(getUserByEmail(user.getEmail())!= null) {
	         throw new Error("L'email est déjà utilisé");
		 }
		 if(parrain==null){
			 throw new Error("Le pseudo du parrain spécifié n'existe pas, veuillez le corriger ou le retirer");
		 }else{
			 user.setParrain(user.getParrain());
		 }

		 user.setRole("user");
		 userRepository.save(user);
	    }
}
