package com.lbb.lbbback.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "utilisateur")
public class User {

	private int id;
	
	private String nom;
	
	private String prenom;
	
	@Column(name="date_naissance")
	private Date dateNaissance;
	
	private String email;
	
	private String pseudo;

	private String parrain;
	@Column(name="avatar_url")
	private String avatarUrl;
	
	private String password;
	
	@Column(name="paris_gagnes")
	private int parisGagnes;
	
	@Column(name="paris_perdus")
	private int parisPerdus;
	
	private String iban;
	
	private String role;
	

	public User() {
		super();
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getParisGagnes() {
		return parisGagnes;
	}

	public void setParisGagnes(int parisGagnes) {
		this.parisGagnes = parisGagnes;
	}

	public int getParisPerdus() {
		return parisPerdus;
	}

	public void setParisPerdus(int parisPerdus) {
		this.parisPerdus = parisPerdus;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getParrain() {
		return parrain;
	}

	public void setParrain(String parrain) {
		this.parrain = parrain;
	}
}
