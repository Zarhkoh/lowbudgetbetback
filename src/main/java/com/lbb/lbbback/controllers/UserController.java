package com.lbb.lbbback.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.lbb.lbbback.models.User;
import com.lbb.lbbback.services.UserService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/users")
public class UserController {
	@Autowired
	UserService userService;

	// Route pour avoir la liste des utilisateurs
	@GetMapping("")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
	
	// Route pour vérifier la connexion de l'utilisateur
	@PostMapping("/login")
	public ResponseEntity<?> userLogin(@RequestBody User user){
		try {
			User existUser = userService.getUserByEmailAndPassword(user.getEmail(),user.getPassword());
			return new ResponseEntity<>(existUser, HttpStatus.OK);
		}catch (Error e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	
    @PostMapping(value = "/register")
    public ResponseEntity<?> createUser(@RequestBody User user) {
    	try{
    		userService.saveUser(user);
    		return new ResponseEntity<User>(user, HttpStatus.OK);
    	}catch (Error e) {
    		return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
    	}
    }

	// Route pour trouver un utilisateur avec son ID
	@GetMapping("/user/{id}")
	public ResponseEntity<User> getUserById(@PathVariable Integer id) {
		try {
			User user = userService.getUserById(id);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
	}


	


}
